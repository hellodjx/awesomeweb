package middlewares

import (
	"example.com/awesome/utils"
	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
	"net/http"
)

func AuthorizeJWTMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		token, err := utils.GetToken(c)
		if err != nil {
			log.Warning(err.Error())
			c.AbortWithStatus(http.StatusUnauthorized)
			return
		}
		if token.Valid {
			id, _ := utils.GetIdFromToken(token)
			c.Set("user_id", id)
			c.Next()
			return
		} else {
			c.AbortWithStatus(http.StatusUnauthorized)
			return
		}
	}
}
