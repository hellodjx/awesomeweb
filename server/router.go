package server

import (
	"example.com/awesome/controllers"
	"example.com/awesome/docs"
	"example.com/awesome/middlewares"
	"example.com/awesome/service"
	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
)

func NewRouter() *gin.Engine {
	router := gin.New()
	router.Use(gin.Logger())
	router.Use(gin.Recovery())

	service.ExampleClient()

	docs.SwaggerInfo.BasePath = "/v1"
	router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
	v1 := router.Group("v1")
	{
		login := new(controllers.LoginController)
		user := new(controllers.UserController)
		item := new(controllers.ItemController)

		v1.POST("/login", login.Login)
		v1.POST("/register", user.Add)

		userGroup := v1.Group("user")
		userGroup.Use(middlewares.AuthorizeJWTMiddleware())
		{
			userGroup.GET("/", user.GetCurrent)
			userGroup.GET("/:id", user.Get)
			userGroup.DELETE("/", user.Delete)
			userGroup.PATCH("/name/:name", user.ChangeName)
			userGroup.PATCH("/password/:password", user.ChangePassword)
		}

		itemGroup := v1.Group("item")
		itemGroup.GET("/:id", item.Get)
		itemGroup.Use(middlewares.AuthorizeJWTMiddleware())
		{
			itemGroup.POST("/", item.Add)
			itemGroup.DELETE("/:id", item.Delete)
			itemGroup.PATCH("/", item.Update)
		}
	}
	return router
}
