package server

import "example.com/awesome/db"
import "example.com/awesome/models"

func Run() {
	db.DB.AutoMigrate(&models.User{})
	db.DB.AutoMigrate(&models.Item{})
	r := NewRouter()
	r.Run()
}
