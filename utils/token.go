package utils

import (
	"errors"
	"example.com/awesome/service"
	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt"
	log "github.com/sirupsen/logrus"
)

func GetBearerSchema() string {
	return "Bearer "
}

func GetToken(c *gin.Context) (*jwt.Token, error) {
	BearerSchema := GetBearerSchema()
	authHeader := c.GetHeader("Authorization")
	if len(authHeader) <= len(BearerSchema) {
		return nil, errors.New("authorization header len is too short, it's invalid")
	}
	tokenString := authHeader[len(BearerSchema):]
	log.Info(tokenString)
	token, err := service.JWTAuthService().ValidateToken(tokenString)
	return token, err
}

func GetIdFromToken(token *jwt.Token) (string, error) {
	if claims, ok := token.Claims.(*service.AuthCustomClaims); ok {
		return claims.StandardClaims.Id, nil
	}
	return "", errors.New("can't get id from token")
}
