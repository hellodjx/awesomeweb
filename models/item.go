package models

import (
	"example.com/awesome/db"
)

type Item struct {
	ID     string `json:"item_id,omitempty"`
	Name   string `json:"name"`
	UserID string `swaggerignore:"true"`
}

func (i Item) GetByID(id string) (*Item, error) {
	var item Item
	result := db.DB.Take(&item, "id = ?", id)
	if result.Error != nil {
		return nil, result.Error
	}

	return &item, nil
}

func (i Item) Add() error {
	result := db.DB.Create(&i)
	return result.Error
}

func (i Item) DeleteByID(id string) error {
	var item *Item
	result := db.DB.Where("ID = ?", id).Delete(&item)
	return result.Error
}

func (i Item) Change() error {
	result := db.DB.Save(&i)
	return result.Error
}
