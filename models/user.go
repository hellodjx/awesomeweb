package models

import (
	"example.com/awesome/db"
)

type User struct {
	ID         string `json:"user_id" gorm:"primary_key"`
	Name       string `json:"name"`
	Email      string `json:"email" gorm:"primary_key"`
	Password   string
	PhotoURL   string `json:"photo_url"`
	SignUpTime int64  `json:"signup_time"`
}

func (u User) GetByID(id string) (*User, error) {
	var user User
	result := db.DB.Take(&user, "ID = ?", id)
	if result.Error != nil {
		return nil, result.Error
	}

	return &user, nil
}

func (u User) GetByEmail(email string) (*User, error) {
	var user User
	result := db.DB.Take(&user, "Email = ?", email)
	if result.Error != nil {
		return nil, result.Error
	}

	return &user, nil
}

func (u User) Add() error {
	result := db.DB.Create(&u)
	return result.Error
}

func (u User) DeleteByID(id string) error {
	var user *User
	result := db.DB.Where("ID = ?", id).Delete(&user)
	return result.Error
}

func (u User) Change() error {
	result := db.DB.Save(&u)
	return result.Error
}
