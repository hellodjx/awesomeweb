package controllers

import (
	"example.com/awesome/service"
	"example.com/awesome/utils"
	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
	"net/http"
)

type LoginCredentials struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

type LoginController struct {
}

// @Summary login for token
// @Param data body LoginCredentials true "login data"
// @Produce json
// @Success 200
// @Router /login [post]
func (controller *LoginController) Login(ctx *gin.Context) {
	var credential LoginCredentials
	err := ctx.ShouldBind(&credential)
	if err != nil {
		ctx.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"message": "bad request"})
		return
	}
	log.Info(credential.Email)

	user, err := userModel.GetByEmail(credential.Email)
	if err != nil {
		ctx.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"message": "Email is not exist"})
		return
	}
	if credential.Password == user.Password {
		token := service.JWTAuthService().GenerateToken(user.ID, user.Email, user.Name)
		ctx.Writer.Header().Set("Authorization", utils.GetBearerSchema()+token)
		ctx.JSON(http.StatusOK, gin.H{"token": utils.GetBearerSchema()+token})
		return
	} else {
		ctx.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"message": "Email or password is incorrect"})
		return
	}
}
