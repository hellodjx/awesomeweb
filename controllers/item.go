package controllers

import (
	"example.com/awesome/models"
	"fmt"
	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
	"net/http"
)

type ItemController struct{}

var itemModel = new(models.Item)

// @Summary get a item
// @Param id path string true "item ID"
// @Produce json
// @Success 200 {object} models.Item
// @Router /item/{id} [get]
func (i ItemController) Get(c *gin.Context) {
	fmt.Println("id: ", c.Param("id"))
	if c.Param("id") != "" {
		item, err := itemModel.GetByID(c.Param("id"))
		if err != nil {
			log.Warning(err.Error())
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"message": "Error to retrieve item"})
			return
		}
		c.JSON(http.StatusOK, gin.H{"item": item})
		return
	}
	c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"message": "bad request"})
	return
}

// @Summary add a item
// @Param data body models.Item true "item data"
// @Security BearerAuth
// @Accept json
// @Produce json
// @Success 200
// @Router /item [post]
func (i ItemController) Add(c *gin.Context) {
	var item models.Item
	if err := c.ShouldBindJSON(&item); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"message": "bad request"})
		return
	}
	userId, _ := c.Get("user_id")
	item.UserID = fmt.Sprintf("%v", userId)
	if err := item.Add(); err != nil {
		log.Error(err.Error())
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"message": "Error to add item"})
		return
	}
	c.JSON(http.StatusOK, gin.H{"message": "OK"})
}

// @Summary delete a item
// @Param id path string true "item ID"
// @Security BearerAuth
// @Produce json
// @Success 200
// @Router /item/{id} [delete]
func (i ItemController) Delete(c *gin.Context) {
	if c.Param("id") != "" {
		err := itemModel.DeleteByID(c.Param("id"))
		if err != nil {
			log.Error(err.Error())
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"message": "Error to delete item"})
			return
		}
		c.JSON(http.StatusOK, gin.H{"message": "OK"})
		return
	}
	c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"message": "bad request"})
	return
}

// @Summary update a item
// @Param data body models.Item true "item data"
// @Security BearerAuth
// @Accept json
// @Produce json
// @Success 200
// @Router /item [patch]
func (i ItemController) Update(c *gin.Context) {
	var item models.Item
	if err := c.ShouldBindJSON(&item); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"message": "bad request"})
		return
	}
	if err := item.Change(); err != nil {
		log.Error(err.Error())
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"message": "Error to update item"})
		return
	}
	c.JSON(http.StatusOK, gin.H{"message": "OK"})
}
