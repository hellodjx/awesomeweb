package controllers

import (
	"example.com/awesome/models"
	"example.com/awesome/service"
	"example.com/awesome/utils"
	"github.com/gin-gonic/gin"
	"github.com/satori/go.uuid"
	log "github.com/sirupsen/logrus"
	"net/http"
	"time"
)

type UserController struct{}

var userModel = new(models.User)

func (u UserController) Get(c *gin.Context) {
	if c.Param("id") != "" {
		user, err := userModel.GetByID(c.Param("id"))
		if err != nil {
			log.Error(err.Error())
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"message": "Error to retrieve user"})
			return
		}
		c.JSON(http.StatusOK, gin.H{"user": user})
		return
	}
	c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"message": "bad request"})
	return
}

func (u UserController) GetCurrent(c *gin.Context) {
	token, err := utils.GetToken(c)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"message": "Error to retrieve user"})
		return
	}
	id := token.Claims.(*service.AuthCustomClaims).Id
	user, err := userModel.GetByID(id)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"message": "Error to retrieve user"})
		return
	}
	c.JSON(http.StatusOK, gin.H{"user": user})
	return
}

func (u UserController) Add(c *gin.Context) {
	name, isNameExist := c.GetPostForm("name")
	email, isEmailExist := c.GetPostForm("email")
	password, isPasswordExist := c.GetPostForm("password")
	log.Info(name)
	id := uuid.NewV4()

	if isNameExist && isPasswordExist && isEmailExist {
		user := models.User{ID: id.String(), Name: name, Password: password,
			SignUpTime: time.Now().Unix()}
		err := user.Add()
		if err != nil {
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"message": "Error to create user"})
			return
		}
		token := service.JWTAuthService().GenerateToken(id.String(), email, name)
		c.Writer.Header().Set("Authorization", "Bearer: "+token)
		c.JSON(http.StatusOK, gin.H{"token": token})
		return
	} else {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"message": "name or password is None"})
		return
	}
}

func (u UserController) Delete(c *gin.Context) {
	token, err := utils.GetToken(c)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"message": "Error to delete user"})
		return
	}
	id := token.Claims.(*service.AuthCustomClaims).Id
	err = userModel.DeleteByID(id)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"message": "Error to delete user"})
		return
	}
}

func (u UserController) ChangeName(c *gin.Context) {
	token, err := utils.GetToken(c)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"message": "Error to change user name"})
		return
	}
	id := token.Claims.(*service.AuthCustomClaims).Id
	user, err := userModel.GetByID(id)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"message": "Error to retrieve user"})
		return
	}
	user.Name = c.Param("name")
	err = user.Change()
	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"message": "Error to change user name"})
		return
	}
}

func (u UserController) ChangePassword(c *gin.Context) {
	token, err := utils.GetToken(c)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"message": "Error to change user password"})
		return
	}
	id := token.Claims.(*service.AuthCustomClaims).Id
	user, err := userModel.GetByID(id)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"message": "Error to retrieve user"})
		return
	}
	newPassword := c.Param("password")
	user.Password = newPassword
	err = user.Change()
	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"message": "Error to change user password"})
		return
	}
}
