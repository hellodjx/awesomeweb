package main

import (
	"example.com/awesome/db"
	"example.com/awesome/server"
)

// @title 测试
// @version 0.1
// @description 测试内容

// @license.name APACHE 2.0
// @license.url http://www.apache.org/licenses/LICENSE-2.0.html
// @securityDefinitions.apikey BearerAuth
// @in header
// @name Authorization
func main() {
	db.Connect()
	server.Run()
}
